package com.kafka.verificaacesso.model;

import java.util.Random;

public class Acesso {
    private String cliente;
    private Long porta;
    private boolean acessoPorta;

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Long getPorta() {
        return porta;
    }

    public void setPorta(Long porta) {
        this.porta = porta;
    }

    public boolean isAcessoPorta() {
        return acessoPorta;
    }

    public void setAcessoPorta(boolean acessoPorta) {
        this.acessoPorta = acessoPorta;
    }
}
