package com.kafka.verificaacesso.producer;

import com.kafka.verificaacesso.model.Acesso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AcessoProducer {

    @Autowired
    private KafkaTemplate<String, Acesso> producer;

    public void enviarAoKafka(Acesso acesso) {
        producer.send("spec2-joao-augusto-1", acesso);
    }

}
