package com.kafka.verificaacesso.controller;

import com.kafka.verificaacesso.model.Acesso;
import com.kafka.verificaacesso.producer.AcessoProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
public class AcessoController {

    @Autowired
    private AcessoProducer acessoProducer;

    @PostMapping("/acesso/{cliente}/{porta}")
    public void create(@PathVariable String cliente, @PathVariable Long porta) {
        Random random = new Random();

        System.out.println("Cliente "+ cliente);
        System.out.println("Porta " + porta);

        Acesso acesso = new Acesso();
        acesso.setCliente(cliente);
        acesso.setPorta(porta);
        acesso.setAcessoPorta(random.nextBoolean());

        acessoProducer.enviarAoKafka(acesso);
    }
}
