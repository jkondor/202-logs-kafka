package com.kafka.lerlogacesso;

import com.kafka.verificaacesso.model.Acesso;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class LerLogAcessoConsumer {

    @KafkaListener(topics = "spec2-joao-augusto-1", groupId = "kondor")
    public void receber(@Payload Acesso acesso) {
        //System.out.println("Recebi cliente " + acesso.getCliente() + " na porta " + acesso.getPorta() + "Acesso: " + acesso.isAcessoPorta());
       if (acesso.isAcessoPorta()){
           System.out.println("Acesso liberado para o cliente " + acesso.getCliente() + " na porta " + acesso.getPorta() );
       }else{
           System.out.println("Acesso não liberado para o cliente " + acesso.getCliente() + " na porta " + acesso.getPorta() );
       }

    }
}
